#!/usr/bin/env python
# coding:utf-8
'''
@Auth ： Just
@File : lottery.py
@Date : 2018/8/29
'''
from sqlalchemy import Column, Integer, BigInteger, VARCHAR, TIMESTAMP, func
from config import Base
from config import CurrentConfig as Config


class Lottery(Base):
	__tablename__ = Config.lottery_table_name
	id = Column(Integer, autoincrement=True, primary_key=True, comment=u'编号')
	trade_type = Column(VARCHAR(20), nullable=False, comment=u'交易类型(购买或出售)')
	coin_name = Column(VARCHAR(20), nullable=False, comment=u'虚拟币类型')
	sell_price_first = Column(VARCHAR(100), nullable=False, comment=u'卖一价')
	sell_tradeCount_first = Column(VARCHAR(100), nullable=False, comment=u'卖一量')
	sell_price_second = Column(VARCHAR(100), nullable=False, comment=u'卖二价')
	sell_tradeCount_second = Column(VARCHAR(100), default=0, comment=u'卖二量')
	buy_price_first = Column(VARCHAR(100), nullable=False, comment=u'买一价')
	buy_tradeCount_first = Column(VARCHAR(100), nullable=False, comment=u'买一量')
	buy_price_second = Column(VARCHAR(100), default=0, comment=u'买二价')
	buy_tradeCount_second = Column(VARCHAR(100), default=0, comment=u'买二量')
	create_time = Column(TIMESTAMP(True), server_default=func.now(), comment=u'创建时间')


