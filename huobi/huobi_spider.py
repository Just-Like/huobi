#!/usr/bin/env python
# coding:utf-8
'''
@Auth ： Just
@File : douyu_lottery.py
@Date : 2018/8/29
'''

import sys
import json
import re
import time
import gevent
import math
from gevent import monkey
monkey.patch_all()
from huobi_config import HBConfig as Config
from huobi_logger import HUOBI_LOG as LOG
from base_lottery import BaseLottery



reload(sys)
sys.setdefaultencoding('utf-8')


class HuobiLottery(BaseLottery):
	def __init__(self, tradeType):
		BaseLottery.__init__(self, Config, LOG)
		self.start_time = time.time()
		self.tradeType = tradeType

	def run(self):
		for coinName, coinId in Config.coinId_dic.iteritems():
			self.crawl(coinName, coinId)

	def crawl(self,coinName, coinid):
		num = ['first','second']
		self.coin_name = coinName
		for type in ['sell','buy']:
			url = Config.data_url.format(coinId=coinid,tradeType=type,currPage=1)
			res = self.scrapy(url)
			for index, data in enumerate(res.json()['data'][:2]):
				setattr(self, type+"_price_"+num[index], data['price'])
				setattr(self, type+"_tradeCount_"+num[index], data['tradeCount'])
		self.format_datas()
		time.sleep(10)















if __name__ == '__main__':
		t = HuobiLottery('sell')
		start_time = time.time()
		t.run()
		# t.update_lottery()
		# time.sleep(60)







