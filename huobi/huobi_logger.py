#!/usr/bin/env python
# coding:utf-8
'''
@Auth ： Just
@File : douyu_logger.py
@Date : 2018/8/29
'''
from huobi.huobi_config import HBConfig
from logging import handlers,Formatter,getLogger

huobi_handler = handlers.RotatingFileHandler(HBConfig.log_file, maxBytes=50 * 1024 * 1024, backupCount=10)
fmt = '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)s - %(message)s'

formatter = Formatter(fmt)
huobi_handler.setFormatter(formatter)

HUOBI_LOG = getLogger('huobi')
HUOBI_LOG.addHandler(huobi_handler)
HUOBI_LOG.setLevel(HBConfig.log_level)