#!/usr/bin/env python
# coding:utf-8
'''
@Auth ： Just
@File : douyu_config.py
@Date : 2018/8/29
'''
import config
import logging


class HBConfig(config.CurrentConfig):
	"""平台配置文件"""

	# log
	log_file = 'log/huobi.log'
	log_level = logging.INFO

	# api

	data_url = "https://otc-api.eiijo.cn/v1/data/trade-market?coinId={coinId}&currency=1&tradeType={tradeType}&currPage={currPage}&payMethod=0&country=37&blockType=general&online=1&range=0&amount="
	crawl_frequency = 180

	coinId_dic = {
		"btc": '1',
		"ETH": '3',
		"USDT": '2',
		"EOS": '5',
		"XRP": '7',
		"LTC": '8',
		'HT': '4',
		'HUSD': '6'
	}